<?php

/**
 * Registers the stylesheet for the default template.
 *
 * @uses wp_enqueue_style
 * @return void
 */

add_action( 'wp_enqueue_scripts', 'rcno_default_styles' );
add_action( 'wp_enqueue_scripts', 'rcno_default_scripts' );

function rcno_default_styles() {
	wp_enqueue_style( 'rcno-default-style', plugin_dir_url( __FILE__ ) . 'default-style.css', array(), '1.0.0', 'all' );
}

function rcno_default_scripts() {
	wp_enqueue_script( 'rcno-default-script', plugin_dir_url( __FILE__ ) . 'default-script.js', array( 'jquery' ), '1.0.0', true );
}

$review_list_url_override = Rcno_Reviews_Option::get_option( 'rcno_reviews_override_review_list_url', '' );
if ($review_list_url_override !== '') {
    // add the code below to your child theme's functions.php file.
    function fwps_term_link_filter( $url, $term, $taxonomy ) {
        // change the industry to the name of your taxonomy
        if ( strstr($taxonomy,  'rcno_') !== FALSE ) {
            $_taxonomy = get_taxonomy( $taxonomy );
            $review_list_url_override = Rcno_Reviews_Option::get_option( 'rcno_reviews_override_review_list_url', '' );
            $_tax = strtolower($_taxonomy->label);
            $_term = $term->name;
            $url_end = str_replace('{{tax}}', $_tax, $review_list_url_override);
            $url_end = str_replace('{{term}}', $_term, $url_end);
            $url = home_url() . '/' . $url_end;
        }
        return $url;
    }
    add_filter( 'term_link', 'fwps_term_link_filter', 10, 3 );
}