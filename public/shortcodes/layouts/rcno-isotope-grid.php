<?php

/**
 * Render a list of all book reviews as an Isotope grid
 *
 * Available variables:
 * array $posts
 * array $options
 * Rcno_Isotope_Grid_Shortcode $this
 *
 * Available functions:
 * sort_by_title()
 */

// Create an output variable containing initial responsive styling
$out = '
        <style>
            .rcno-isotope-grid-item {
                width: 200px; height: 250px; position: absolute; left: 0; top: 0;
            }
            .rcno-isotope-grid-item-caption {
                position: absolute;
                top: 0;
                width: 100%;
                max-height: 100%;
                overflow: auto;
                padding: 10px 10px 30px 9px;
                line-height: 22px;
                color: #fff;
                text-align: center;
                font-size: 13px;
                background: linear-gradient(0deg,rgba(0,0,0,.7),rgba(0,0,0,.3) 70%,transparent);
            }
            
            @media (max-width: 768px) {
                .rcno-isotope-grid-item {
                    width: 160px; height: 200px;
                }
            }


            @media (max-width: 480px) {
                .rcno-isotope-grid-item {
                    width: 120px; height: 150px;
                }
            }
            @media (max-width: 320px) {
                .rcno-isotope-grid-item {
                    width: 108px; height: 135px;
                }
            }
        </style>';

if ( $posts && count( $posts ) > 0 ) {
    if ($options['infinite']) {
        wp_register_script('infinite-scroll', plugin_dir_url(__FILE__) . '../../js/infinite-scroll.pkgd.min.js', array(), '3.0.6', true);
    }

    // Create an index i to compare the number in the list and check for first and last item.
	$i = 0;

	// Create an empty array to take the book details.
	$books = array();

	// Loop through each post, book title from post-meta and work on book title.
    if (!$options['infinite']) {
        foreach ( $posts as $book ) {
            $book_details   = get_post_custom( $book->ID );
            $unsorted_title = $book_details['rcno_book_title'][0];
            $sorted_title   = $unsorted_title;
            if ( $this->variables['ignore_articles'] ) {
                $sorted_title = preg_replace( '/^(' . $this->variables['articles_list'] . ') (.+)/', '$2, $1', $book_details['rcno_book_title'] );
            }
            $books[] = array(
                'ID'             => $book->ID,
                'sorted_title'   => $sorted_title,
                'unsorted_title' => $unsorted_title,
            );
            usort( $books, array( $this, 'sort_by_title' ) );
        }
    }

	$select = '';
	if ( $options['selectors'] ) {
		$exclude = explode( ',', strtolower( $options['exclude'] ) );
		$hide = explode(',', strtolower( $options['hide']));
		$select .= '<div class="rcno-isotope-grid-select-container">';

		if ( $options['search'] ) {
			$select .= '<input type="text" class="rcno-isotope-grid-search rcno-isotope-grid-select-wrapper" placeholder="'
						. __( 'Search...', 'recencio-book-reviews' ) . '" />';
		}

		$taxonomies = array_diff( $this->variables['custom_taxonomies'], $exclude );
		foreach ( $taxonomies as $taxonomy ) {
			$terms     = get_terms( 'rcno_' . strtolower( $taxonomy ), 'orderby=name&order=ASC&hide_empty=1' );
			$_taxonomy = get_taxonomy( 'rcno_' . strtolower( $taxonomy ) ); // Using the label to avoid i18n issues.
			$select   .= '<div class="rcno-isotope-grid-select-wrapper';
            if (in_array($taxonomy, $hide)) {
                $select   .= ' hide';
            }
            $select   .= '">';
			$select   .= '<label for="rcno-isotope-grid-select">' . $_taxonomy->label . '</label>';
            $select   .= '<select class="rcno-isotope-grid-select" name="rcno-isotope-grid-select" id="'.strtolower($_taxonomy->label).'-select">';
			$select   .= '<option value="*">' . sprintf( '%s %s', __( 'Select', 'recencio-book-reviews' ), $_taxonomy->label ) . '</option>';
			foreach ( $terms as $term ) {
				if ( is_object( $term ) ) {
				    if (!$options['infinite']){
				        $select .= '<option value="' . '.' . $term->slug . '">' . $term->name . '</option>';
                    } else {
				        // use taxonomy name with term id for infinite scroll loading as Wordpress API only returns term ids not names
                        $select .= '<option value="' . '.' . strtolower( $taxonomy ) . '-' . $term->term_id . '">' . $term->name . '</option>';
                    }
				}
			}
			$select .= '</select>';
			$select .= '</div>';
		}

		if ( $options['rating'] ) {
			$select .= '<div class="rcno-isotope-grid-select-wrapper">';
			$select .= '<label for="rcno-isotope-grid-select">' . __( 'Rating', 'recencio-book-reviews' ) . '</label>';
			$select .= '<select class="rcno-isotope-grid-select" name="rcno-isotope-grid-select" id="ratings-select">';
			$select .= '<option value="*">' . __( 'Select Rating', 'recencio-book-reviews' ) . '</option>';
			$select .= '<option value=".1">' . __( '1 Star', 'recencio-book-reviews' ) . '</option>';
			$select .= '<option value=".2">' . __( '2 Stars', 'recencio-book-reviews' ) . '</option>';
			$select .= '<option value=".3">' . __( '3 Stars', 'recencio-book-reviews' ) . '</option>';
			$select .= '<option value=".4">' . __( '4 Stars', 'recencio-book-reviews' ) . '</option>';
			$select .= '<option value=".5">' . __( '5 Stars', 'recencio-book-reviews' ) . '</option>';
			$select .= '</select>';
			$select .= '</div>';
		}

		$select .= '<span class="rcno-isotope-grid-select reset">&olarr;</span>';
		$select .= '</div>';
	}

    if (!$options['infinite']) {

        $out .= '<div id="rcno-isotope-grid-container" class="rcno-isotope-grid-container">';

        // Walk through all the books to build alphabet navigation.
        foreach ($books as $book) {

            // Add the entry for the post.
            $out .= '<div class="rcno-isotope-grid-item ';
            $out .= sanitize_title($this->template->get_the_rcno_book_meta($book['ID'], 'rcno_book_title', '', false)) . ' ';
            $out .= implode(' ', $this->template->get_rcno_review_html_classes($book['ID'])) . '" ';
            $out .= '>';
            if ($options['rating']) {
                $out .= $this->template->get_the_rcno_admin_book_rating($book['ID']);
            }
            $out .= '<a href="' . get_permalink($book['ID']) . '">';
            // $size 'thumbnail', 'medium', 'full', 'rcno-book-cover-sm', 'rcno-book-cover-lg'.
            if ((int)$options['width'] > 85) {
                $out .= $this->template->get_the_rcno_book_cover($book['ID'], 'rcno-book-cover-lg', true);
            } else {
                $out .= $this->template->get_the_rcno_book_cover($book['ID'], 'rcno-book-cover-sm', true);
            }
            $out .= '</a>';
            $out .= '</div>';

            // Increment the counter.
            $i++;
        }
        // Close the last list.
        $out .= '</div>';
    } else {
        // add item template
        $out .= '<!-- .grid-item template HTML -->';
        $out .= '<script type="text/html" id="grid-item-template">';
        $out .= '<div class="rcno-isotope-grid-item {{slug}} ';
        $out .= implode(' ', $this->template->get_rcno_review_html_classes_infinite_template()) . '" ';
        $out .= '>';
        if ($options['rating']) {
            $out .= $this->template->get_the_rcno_admin_book_rating_template();
        }
        $out .= '<a href="{{link}}">';
        $out .= '{{book_cover}}';
        $out .= '</a>';
        $out .= '<div class="rcno-isotope-grid-item-caption" onclick="location.href=\'{{link}}\';" style="cursor: pointer;">{{book_title}}</div>';
        $out .= '</div>';
        $out .= '</script>';

        $out .= '<div id="rcno-isotope-grid-container" class="rcno-isotope-grid-container">';
        $out .= '</div>';

        $out .= '<div class="page-load-status">';
        $out .= '  <div class="loader-ellips infinite-scroll-request">';
        $out .= '    <span class="loader-ellips__dot"></span>';
        $out .= '    <span class="loader-ellips__dot"></span>';
        $out .= '    <span class="loader-ellips__dot"></span>';
        $out .= '    <span class="loader-ellips__dot"></span>';
        $out .= '  </div>';
//        $out .= '  <p class="infinite-scroll-last">End of content</p>';
//        $out .= '  <p class="infinite-scroll-error">No more pages to load</p>';
        $out .= '</div>';
    }

	// This is where everything is printed.
	// We are checking if we are on a page because the following error occurs in production, sometimes.
	// Warning: Cannot modify header information - headers already sent.
	if ( is_singular() ) {
		echo $select . $out;
	}
} else {
	// No book reviews.
	esc_html_e( 'There are no book reviews to display.', 'recencio-book-reviews' );
}
