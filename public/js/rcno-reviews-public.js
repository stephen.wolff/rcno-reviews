/* globals */
let filterPathString = "";
let ratingFilterString = "";
let infiniteScrollPath;

(function ($) {
  'use strict';

  /**
   * All of the code for your public-facing JavaScript source
   * should reside in this file.
   *
   * Note: It has been assumed you will write jQuery code here, so the
   * $ function reference has been prepared for usage within the scope
   * of this function.
   *
   * This enables you to define handlers, for when the DOM is ready:
   *
   * $(function() {
   *
   * });
   *
   * When the window is loaded:
   *
   * $( window ).load(function() {
   *
   * });
   *
   * ...and/or other possibilities.
   *
   * Ideally, it is not considered best practise to attach more than a
   * single DOM-ready or window-load handler for a particular page.
   * Although scripts in the WordPress core, Plugins and Themes may be
   * practising this, we should strive to set a better example in our own work.
   */
  $(function () {
    if (window.InfiniteScroll === undefined) {
      if (typeof owl_carousel_options !== 'undefined') {

        $('.rcno-book-slider-container.owl-carousel').owlCarousel({
          items: 1,
          autoplay: true,
          autoplayTimeout: owl_carousel_options.duration * 1000,
          loop: true
        });
      }

      if (window.Macy !== undefined) {
        Macy({
          container: '#macy-container',
          trueOrder: false,
          waitForImages: false,
          margin: 10,
          columns: 4,
          breakAt: {
            1200: 4,
            940: 3,
            520: 2,
            400: 1
          }
        });
      }

      showStars();

      if (window.Isotope !== undefined) {

        var qsRegex;
        var $grid = $('.rcno-isotope-grid-container').imagesLoaded(function () {
          // init Isotope after all images have loaded
          $grid.isotope({
            itemSelector: '.rcno-isotope-grid-item',
            layoutMode: 'masonry'
          });
        });
        var select = $('.rcno-isotope-grid-select');
        select.on('change', function () {
          var filterValue = $(this).val();
          $grid.isotope({filter: filterValue});
          select.not($(this)).prop('selectedIndex', 0);
        });
        $('.rcno-isotope-grid-select.reset').on('click', function () {
          $grid.isotope({filter: '*'});
          select.each(function () {
            $(this).prop('selectedIndex', 0);
          });
          $('.rcno-isotope-grid-search').val('');
        });

        var $search = $('.rcno-isotope-grid-search').keyup(debounce(function () {
          qsRegex = new RegExp($search.val(), 'gi');
          $grid.isotope({
            filter: function () {
              return qsRegex ? $(this).context.className.match(qsRegex) : true;
            }
          });
        }, 200));
      }
    } else {
      infiniteScrollPath = function() {
        let path = "";
        if (ratingFilterString !== "") {
          path = "/wp-json/rcno-review/v1/rating/" + ratingFilterString + '?page=' + this.pageIndex;
        } else {
          if (filterPathString !== "") {
            path = '/wp-json/wp/v2/rcno/review?page=' + this.pageIndex + "&" + filterPathString;
          } else {
            path = '/wp-json/wp/v2/rcno/review?page=' + this.pageIndex;
          }
        }
        return path;
      };

      let $grid = $('.rcno-isotope-grid-container');
      $grid.isotope({
        itemSelector: '.rcno-isotope-grid-item',
      });

      $grid.infiniteScroll({
        path: infiniteScrollPath,
        itemSelector: '.rcno-isotope-grid-item',
        responseType: 'text',
        loading: {
          finishedMsg: 'All loaded.',
        },
        pageIndex: 1,
        history: false
      });

      $grid.on( 'load.infiniteScroll', function( event, response ) {
        // parse response into JSON data
        let data = JSON.parse( response );
        // compile data into HTML
        let itemsHTML = data.map( getItemHTML ).join('');
        // convert HTML string into elements
        let $items = $( itemsHTML );

        $items.imagesLoaded( function() {
          // append item elements
          $grid.isotope().append( $items )
            .isotope('appended', $items );
          $grid.isotope('reloadItems');
          $grid.isotope('layout');
          showStars();
        });
      });

      let $search = $('.rcno-isotope-grid-search').keyup( debounce( function() {
        let searchValue = $search.val();
        searchForValue(searchValue);
      }, 200 ) );

      // Filter / dropdowns
      let select = $( '.rcno-isotope-grid-select' );
      select.on( 'change', function() {

        // get all dropdown values
        let sel = $('#' + this.id);
        let selectedText = sel[0].options[sel[0].selectedIndex].text;
        let selectedValue = sel[0].options[sel[0].selectedIndex].value;
        let selectKey = this.id.substr(0, this.id.length-7);
        if (selectKey === 'ratings') {
          selectedText = selectedText.substr(0, 1);
        }
        if (selectedText.search('Select') !== -1) {
          window.location.hash = '';
          filterPathString = '';
          filterGrid();

        } else {

          window.location.hash = '#!' + selectKey + '=' + encodeURIComponent(selectedText);
          filterPathString = getFilterPath(selectedValue);
          filterGrid();
          select.not( $( this ) ).prop( 'selectedIndex', 0 );
        }
      } );

      // enable reset button
      $('.rcno-isotope-grid-select.reset').on('click', function() {
        window.location.hash = "";
        filterPathString = '';

        select.each(function() {
          $(this).prop( 'selectedIndex', 0 );
        });
        $('.rcno-isotope-grid-search').val('');
        filterGrid();
      });

      //----------------------------------------//
      // check for any filters in location hash
      let hash = window.location.hash.replace(/^#!/, '');
      let applyFilter = hash !== "";
      let result = hash.split('&').reduce(function (result, item) {
        let parts = item.split('=');
        result[parts[0]] = parts[1];
        return result;
      }, {});

      if (applyFilter) {

        for (let p in result) {
          if (p === 'search') {
            let searchValue = decodeURIComponent(result[p]);
            $('.rcno-isotope-grid-search').val(searchValue);
            searchForValue(searchValue);
          } else {
            let $select = $('#' + p + '-select');
            let $options = $select.find('option');
            let optionLabel = decodeURIComponent(result[p]);
            if (p === 'ratings') {
              optionLabel = optionLabel + ' Stars';
            }

            for (let filterIndex in $options) {
              if ($options.hasOwnProperty(filterIndex) && $options[filterIndex].textContent === optionLabel) {
                $select.prop( 'selectedIndex', filterIndex);
                filterPathString = getFilterPath($options[filterIndex].value);
                filterGrid();

                break;
              }
            }
          }
        }
      } else {
        // sets up isotope without filters
        filterPathString = "";
        filterGrid();
      }


      // templating of items
      let itemTemplateSrc = $('#grid-item-template').html();
      function getItemHTML( photo ) {
        return microTemplate( itemTemplateSrc, photo );
      }

      // micro templating, sort-of
      function microTemplate( src, data ) {
        // replace {{tags}} in source
        return src.replace( /\{\{([\w\-_\.]+)\}\}/gi, function( match, key ) {
          // walk through objects to get value
          let value = data;
          key.split('.').forEach( function( part ) {
            value = value[ part ];
          });
          return value;
        });
      }

      function filterGrid(){
        $grid.isotope('destroy');
        $grid.infiniteScroll('destroy');

        $('.rcno-isotope-grid-item').remove();

        $grid.infiniteScroll({
          path: infiniteScrollPath,
          itemSelector: '.rcno-isotope-grid-item',
          responseType: 'text',
          loading: {
            finishedMsg: 'All loaded.',
          },
          pageIndex: 1,
          history: false
        });

        // load initial page
        $grid.infiniteScroll('loadNextPage');
      }

      function searchForValue(searchValue) {
        // search box
        select.each(function() {
          $(this).prop( 'selectedIndex', 0 );
        });
        window.location.hash = '#!search=' + searchValue;
        filterPathString = "search="+encodeURIComponent(searchValue);
        filterGrid();
      }

      function getFilterPath(selectedValue) {
        let hyphenIndexOf = selectedValue.indexOf('-');
        if (hyphenIndexOf === -1){
          ratingFilterString = selectedValue.substr(1);
          return "";
        } else {
          ratingFilterString = "";
          return "rcno/" + selectedValue.substr(1).replace('-', '=');
        }
      }
    }

    function showStars() {
      let stars = $( '.rcno-admin-rating .stars' );
      $.each(stars, function( index, star ) {
        $(star).starRating({
          initialRating: $(star).data('review-rating'),
          emptyColor: rcno_star_rating_vars.background_colour,
          activeColor: rcno_star_rating_vars.star_colour,
          useGradient: false,
          strokeWidth: 0,
          readOnly: true,
        });
      });
    }

    // Debounce so filtering doesn't happen every millisecond
    function debounce( fn, threshold ) {
      let timeout;
      threshold = threshold || 100;
      return function debounced() {
        clearTimeout( timeout );
        let args = arguments;
        let _this = this;
        function delayed() {
          fn.apply( _this, args );
        }
        timeout = setTimeout( delayed, threshold );
      };
    }
	});

})(jQuery);
